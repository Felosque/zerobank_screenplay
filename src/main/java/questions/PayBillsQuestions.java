package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import userinterface.PayBillsForm;

public class PayBillsQuestions {

    private PayBillsQuestions(){

    }

    public static Question<Boolean> isSuccessfullyTransaction(){
        return actor -> WebElementQuestion.the(PayBillsForm.ALERT_SUCCESSFULLY).answeredBy(actor).isCurrentlyVisible();
    }

}
