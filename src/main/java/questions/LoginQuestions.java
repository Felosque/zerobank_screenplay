package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import userinterface.LoginForm;

public class LoginQuestions {

    private LoginQuestions(){

    }

    public static Question<String> titleLogIn(){
        return actor -> TextContent.of(LoginForm.LBL_TITLE).answeredBy(actor).trim();
    }
}
