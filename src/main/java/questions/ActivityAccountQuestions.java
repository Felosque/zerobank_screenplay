package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import userinterface.AccountActivityForm;

public class ActivityAccountQuestions {

    private ActivityAccountQuestions() {
    }

    public static Question<String> tableMessageError(){
        return actor -> TextContent.of(AccountActivityForm.ALERT_ERROR).answeredBy(actor).trim();
    }

    public static Question<String> transactionDescription(){
        return actor -> TextContent.of(AccountActivityForm.DESC_TRANSACTION).answeredBy(actor).trim();
    }

    public static Question<Boolean> isTableTransactionsVisible(){
        return actor -> WebElementQuestion.the(AccountActivityForm.TABLE_RESULTS).answeredBy(actor).isCurrentlyVisible();
    }
}
