package questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import userinterface.TransferFundForm;

public class TransferFundQuestions {

    private TransferFundQuestions(){}

    public static Question<Boolean> isSuccessfullyTransaction(){
        return actor -> WebElementQuestion.the(TransferFundForm.ALERT_SUCCESSFULLY).answeredBy(actor).isCurrentlyVisible();
    }


}
