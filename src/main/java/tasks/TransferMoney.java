package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import userinterface.TransferFundForm;
import utils.Utils;

public class TransferMoney implements Task {

    public final String fromAccount;
    public final String toAccount;
    public final String amount;
    public final String description;

    public TransferMoney(String fromAccount, String toAccount, String amount, String description) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
        this.description = description;
    }

    public static Performable with(String fromAccount, String toAccount, String amount, String description){
        return Tasks.instrumented(TransferMoney.class, fromAccount, toAccount, amount, description);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        //List<WebElementFacade> elements =
        actor.attemptsTo(
                Enter.theValue(amount).into(TransferFundForm.TXT_AMOUNT),
                Enter.theValue(description).into(TransferFundForm.TXT_DESCRIPTION),
                SelectFromOptions.byVisibleText(Utils.getTextByPartialText(TransferFundForm.CB_FROM_ACCOUNT_OPTIONS, fromAccount)).from(TransferFundForm.CB_FROM_ACCOUNT),
                SelectFromOptions.byVisibleText(Utils.getTextByPartialText(TransferFundForm.CB_TO_ACCOUNT_OPTIONS, toAccount)).from(TransferFundForm.CB_TO_ACCOUNT),
                Click.on(TransferFundForm.BTN_CONTINUE),
                Click.on(TransferFundForm.BTN_SUBMIT)
        );
    }
}
