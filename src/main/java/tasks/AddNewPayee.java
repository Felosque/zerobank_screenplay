package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.PayBillsForm;

public class AddNewPayee implements Task {

    private final String payeeName;
    private final String payeeAddress;
    private final String account;
    private final String details;

    public AddNewPayee(String payeeName, String payeeAddress, String account, String details) {
        this.payeeName = payeeName;
        this.payeeAddress = payeeAddress;
        this.account = account;
        this.details = details;
    }

    public static Performable with(String payeeName, String payeeAddress, String account, String details){
        return Tasks.instrumented(AddNewPayee.class, payeeName, payeeAddress, account, details);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(payeeName).into(PayBillsForm.TXT_PAYEE_NAME),
                Enter.theValue(payeeAddress).into(PayBillsForm.TXT_PAYEE_ADDRESS),
                Enter.theValue(account).into(PayBillsForm.TXT_PAYEE_ACCOUNT),
                Enter.theValue(details).into(PayBillsForm.TXT_PAYEE_DETAILS),
                Click.on(PayBillsForm.BTN_ADD_PAYEE)
        );
    }

}
