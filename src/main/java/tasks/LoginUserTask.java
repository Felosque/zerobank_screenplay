package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import userinterface.LoginForm;

public class LoginUserTask implements Task {

    private final String username;
    private final String password;

    public LoginUserTask(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static UserBuilder with(){
        return new UserBuilder();
    }


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(username).into(LoginForm.USERNAME),
                Enter.theValue(password).into(LoginForm.PASSWORD),
                Click.on(LoginForm.BTN_SIGN_IN)
        );
    }


    public static class UserBuilder{
        private String username;
        private String password;

        public UserBuilder and(){
            return this;
        }

        public UserBuilder username(String username){
            this.username = username;
            return this;
        }

        public UserBuilder password(String password){
            this.password = password;
            return this;
        }

        public Performable build(){
            return new LoginUserTask(username, password);
        }
    }
}
