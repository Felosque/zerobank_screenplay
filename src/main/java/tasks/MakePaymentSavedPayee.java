package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import userinterface.PayBillsForm;
import utils.Utils;

public class MakePaymentSavedPayee implements Task {

    private final String payee;

    private final String account;

    private final String amount;

    private final String date;

    private final String description;

    public MakePaymentSavedPayee(String payee, String account, String amount, String date, String description) {
        this.payee = payee;
        this.account = account;
        this.amount = amount;
        this.date = date;
        this.description = description;
    }

    public static Performable with(String payee, String account, String amount, String date, String description){
        return Tasks.instrumented(MakePaymentSavedPayee.class, payee, account, amount, date, description);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(Utils.getTextByPartialText(PayBillsForm.CB_PAYEE_OPTIONS, payee)).from(PayBillsForm.CB_PAYEE),
                SelectFromOptions.byVisibleText(Utils.getTextByPartialText(PayBillsForm.CB_ACCOUNT_OPTIONS, account)).from(PayBillsForm.CB_ACCOUNT),
                Enter.theValue(amount).into(PayBillsForm.TXT_SAVED_AMOUNT),
                Enter.theValue(date).into(PayBillsForm.TXT_SAVED_DATE),
                Enter.theValue(description).into(PayBillsForm.TXT_SAVED_DESCRIPTION),
                Click.on(PayBillsForm.BTN_PAY_SAVED_PAYEE)
        );
    }
}
