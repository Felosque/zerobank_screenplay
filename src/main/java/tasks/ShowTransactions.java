package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import userinterface.AccountActivityForm;

public class ShowTransactions implements Task {

    private final String account;

    public ShowTransactions(String type) {
        this.account = type;
    }

    public static Performable with(String type){
        return Tasks.instrumented(ShowTransactions.class, type);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(account).from(AccountActivityForm.CB_ACCOUNT)
        );
    }
}
