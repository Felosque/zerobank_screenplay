package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import userinterface.PayBillsForm;
import utils.Utils;

public class PurchaseForeignCurrency implements Task {

    private final String currency;

    private final String amount;

    public PurchaseForeignCurrency(String currency, String amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public static Performable with(String currency, String amount){
        return Tasks.instrumented(PurchaseForeignCurrency.class, currency, amount);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                SelectFromOptions.byVisibleText(Utils.getTextByPartialText(PayBillsForm.CB_CURRENCY_OPTIONS, currency)).from(PayBillsForm.CB_CURRENCY),
                Enter.theValue(amount).into(PayBillsForm.TXT_CURRENCY_AMOUNT),
                Click.on(PayBillsForm.RB_CONVERT_DOLLAR),
                Click.on(PayBillsForm.BTN_PURCHASE_CURRENCY)
        );
    }
}
