package tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import userinterface.AccountActivityForm;

public class FindTransactions implements Task {

    private final String description;
    private final String fromDate;
    private final String toDate;
    private final String fromAmount;
    private final String toAmount;
    private final String type;

    public FindTransactions(String description, String fromDate, String toDate, String fromAmount, String toAmount, String type) {
        this.description = description;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.type = type;
    }

    public static Performable with(String description, String fromDate, String toDate, String fromAmount, String toAmount, String type){
        return Tasks.instrumented(FindTransactions.class, description, fromDate, toDate, fromAmount, toAmount, type);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(description).into(AccountActivityForm.TXT_DESCRIPTION),
                Enter.theValue(fromDate).into(AccountActivityForm.TXT_FROM_DATE),
                Enter.theValue(toDate).into(AccountActivityForm.TXT_TO_DATE),
                Enter.theValue(fromAmount).into(AccountActivityForm.TXT_FROM_AMOUNT),
                Enter.theValue(toAmount).into(AccountActivityForm.TXT_TO_AMOUNT),
                SelectFromOptions.byVisibleText(type).from(AccountActivityForm.CB_TYPE),
                Click.on(AccountActivityForm.BTN_FIND)
        );
    }
}
