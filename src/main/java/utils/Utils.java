package utils;

import net.serenitybdd.core.Serenity;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Utils {

    private Utils(){}

    public static String getTextByPartialText(By dropdown, String value){
        List<WebElement> options = Serenity.getDriver().findElements(dropdown);
        String textValue = "NADA";
        for (WebElement option : options){
            if (option.getText().contains(value)){
                textValue = option.getText();
                break;
            }
        }
        return textValue;
    }
}
