package navigation;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import userinterface.AccountActivityForm;
import userinterface.PayBillsForm;
import userinterface.TransferFundForm;

public class NavigateTo {

    private NavigateTo() {
    }

    public static Performable theLoginPage(){
        return Task.where("el usuario abre la pagina de login",
                Open.browserOn().the(LoginPage.class)
        );
    }

    public static Performable theTransferFundsPage(){
        return Task.where("el usuario da clic en el botón transfer funds",
                Click.on(TransferFundForm.TAB_TRANSFER_FUNDS)
        );
    }

    public static Performable thePayBillsTab(){
        return Task.where("el usuario da clic en el botón pay bills",
                Click.on(PayBillsForm.TAB_PAY_BILLS)
        );
    }

    public static Performable thePayBillsAddNewPayee(){
        return Task.where("el usuario da clic en el botón add new payee",
                Click.on(PayBillsForm.TAB_ADD_NEW_PAYEE)
        );
    }

    public static Performable thePayBillsPaySaved(){
        return Task.where("el usuario da clic en el botón pay saved",
                Click.on(PayBillsForm.TAB_PAY_SAVED)
        );
    }

    public static Performable thePayBillsPurchaseCurrency(){
        return Task.where("el usuario da clic en el botón purchase currency",
                Click.on(PayBillsForm.TAB_PURCHASE_CURRENCY)
        );
    }

    public static Performable theActivityAccountTab(){
        return Task.where("el usuario da clic en el botón account tab",
                Click.on(AccountActivityForm.TAB_ACCOUNT_ACTIVITY)
        );
    }

    public static Performable theActivityAccountShowTransactions(){
        return Task.where("el usuario da clic en el botón show transactions",
                Click.on(AccountActivityForm.TAB_SHOW_TRANSACTIONS)
        );
    }

    public static Performable theActivityAccountFindTransactions(){
        return Task.where("el usuario da clic en el botón find transactions",
                Click.on(AccountActivityForm.TAB_FIND_TRANSACTIONS)
        );
    }
}
