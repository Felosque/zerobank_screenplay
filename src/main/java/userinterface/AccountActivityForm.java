package userinterface;

import org.openqa.selenium.By;

public class AccountActivityForm {

    private AccountActivityForm(){}

    public static final By TAB_ACCOUNT_ACTIVITY = By.xpath("//a[contains(text(),'Account Activity')]");
    public static final By TAB_SHOW_TRANSACTIONS = By.xpath("//a[contains(text(),'Show Transactions')]");
    public static final By TAB_FIND_TRANSACTIONS = By.xpath("//a[contains(text(),'Find Transactions')]");

    public static final By CB_ACCOUNT = By.id("aa_accountId");
    public static final By TABLE_RESULTS = By.xpath("//thead/tr/th[1]");
    public static final By ALERT_ERROR = By.xpath("//div[@class='well']");

    public static final By TXT_DESCRIPTION = By.id("aa_description");
    public static final By TXT_FROM_DATE = By.id("aa_fromDate");
    public static final By TXT_TO_DATE = By.id("aa_toDate");
    public static final By TXT_FROM_AMOUNT = By.id("aa_fromAmount");
    public static final By TXT_TO_AMOUNT = By.id("aa_toAmount");
    public static final By CB_TYPE = By.id("aa_type");
    public static final By BTN_FIND = By.xpath("//button[@class='btn btn-primary']");


    public static final By DESC_TRANSACTION = By.xpath("//div[@id='filtered_transactions_for_account']//td[contains(text(),'ONLINE TRANSFER REF #UKKSDRQG6L')]");

}
