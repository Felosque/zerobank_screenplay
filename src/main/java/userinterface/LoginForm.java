package userinterface;

import org.openqa.selenium.By;

public class LoginForm {

    private LoginForm(){}

    public static final By USERNAME = By.id("user_login");
    public static final By PASSWORD = By.id("user_password");
    public static final By BTN_SIGN_IN = By.xpath("//input[@name='submit']");
    public static final By LBL_TITLE = By.xpath("//a[@class='brand']");


}
