package userinterface;

import org.openqa.selenium.By;

public class TransferFundForm {

    private TransferFundForm(){
    }

    public static final By TAB_TRANSFER_FUNDS = By.xpath("//a[contains(text(),'Transfer Funds')]");
    public static final By CB_FROM_ACCOUNT = By.xpath("//select[@id='tf_fromAccountId']");
    public static final By CB_FROM_ACCOUNT_OPTIONS = By.xpath("//select[@id='tf_toAccountId']//option");
    public static final By CB_TO_ACCOUNT = By.xpath("//select[@id='tf_toAccountId']");
    public static final By CB_TO_ACCOUNT_OPTIONS = By.xpath("//select[@id='tf_toAccountId']//option");
    public static final By TXT_AMOUNT = By.id("tf_amount");
    public static final By TXT_DESCRIPTION = By.id("tf_description");
    public static final By BTN_CONTINUE = By.id("btn_submit");
    public static final By BTN_SUBMIT = By.id("btn_submit");
    public static final By ALERT_SUCCESSFULLY =  By.xpath("//div[@class='alert alert-success']");

}
