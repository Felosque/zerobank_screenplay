package userinterface;

import org.openqa.selenium.By;

public class PayBillsForm {

    private PayBillsForm(){}

    //Modules tabs
    public static final By TAB_PAY_BILLS = By.xpath("//a[contains(text(),'Pay Bills')]");
    public static final By TAB_PAY_SAVED  = By.xpath("//a[contains(text(),'Pay Saved Payee')]");
    public static final By TAB_ADD_NEW_PAYEE = By.xpath("//a[contains(text(),'Add New Payee')]");
    public static final By TAB_PURCHASE_CURRENCY = By.xpath("//a[contains(text(),'Purchase Foreign Currency')]");

    // PAY PAYEE
    public static final By CB_PAYEE = By.xpath("//select[@id='sp_payee']");
    public static final By CB_PAYEE_OPTIONS = By.xpath("//select[@id='sp_payee']//option");
    public static final By CB_ACCOUNT_OPTIONS = By.xpath("//select[@id='sp_account']//option");
    public static final By CB_ACCOUNT = By.xpath("//select[@id='sp_account']");
    public static final By TXT_SAVED_AMOUNT = By.id("sp_amount");
    public static final By TXT_SAVED_DATE = By.id("sp_date");
    public static final By TXT_SAVED_DESCRIPTION = By.id("sp_description");
    public static final By BTN_PAY_SAVED_PAYEE = By.id("pay_saved_payees");

    //ADD NEW PAYEE
    public static final By TXT_PAYEE_NAME = By.id("np_new_payee_name");
    public static final By TXT_PAYEE_ADDRESS = By.id("np_new_payee_address");
    public static final By TXT_PAYEE_ACCOUNT = By.id("np_new_payee_account");
    public static final By TXT_PAYEE_DETAILS = By.id("np_new_payee_details");
    public static final By BTN_ADD_PAYEE = By.id("add_new_payee");

    //PURCHASE FOREIGN CURRENCY

    public static final By CB_CURRENCY = By.xpath("//select[@id='pc_currency']");
    public static final By CB_CURRENCY_OPTIONS = By.xpath("//select[@id='pc_currency']//option");
    public static final By TXT_CURRENCY_AMOUNT = By.id("pc_amount");
    public static final By RB_CONVERT_DOLLAR = By.id("pc_inDollars_true");
    public static final By RB_CONVERT_SELECT = By.id("pc_inDollars_false");
    public static final By BTN_CALCULATE_COST = By.id("pc_calculate_costs");
    public static final By LBL_CALCULATE_COST = By.id("pc_conversion_amount");
    public static final By BTN_PURCHASE_CURRENCY = By.id("purchase_cash");

    //ALERTS
    public static final By ALERT_SUCCESSFULLY = By.xpath("//button[@class='close']");

}
