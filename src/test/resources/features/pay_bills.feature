@AllTests
@PayBills
Feature: Pagar, Agregar y comprar moneda extranjera
  I want to use this template for my feature file
	Background: Usuario se loguea en el sistema
		Given yo me autentico  en Zero Bank con el usuario 'username' y la contraseña 'password'

	@PaySavedPayee
	Scenario: Pagar a un beneficiario exitosamente
	 	And  Me dirijo a la pagina de Pay Bills
	  And Selecciono la opcion de  Pay Saved Payee
	  When Ingreso los datos para realizar la transaccion al beneficiario
					| Payee    | Account | Amount | Date       | Description |
					| Apple    | Loan    | 100000 | 2021-11-23 | Any Desc    |
	  And Confirmo la realizacion de la transaccion al beneficiario
	  Then el sistema muestra que la transaccion fue exitosa
	 
	@PaySavedPayee
	Scenario: Pagar a un beneficiario sin incluir el monto
	 	And  Me dirijo a la pagina de Pay Bills
	  And Selecciono la opcion de  Pay Saved Payee
	  When Ingreso los datos para realizar la transaccion al beneficiario
					| Payee    | Account | Amount | Date       | Description |
					| Apple    | Loan    | 			  | 2021-11-23 | Any Desc    |
	  And Confirmo la realizacion de la transaccion al beneficiario
	  Then el sistema muestra un mensaje de error.
	  
	@AddNewPayee
	Scenario: Agregar a un beneficiario exitosamente
		And  Me dirijo a la pagina de Pay Bills
		And Selecciono la opcion de  Add New Payee
   		When Ingreso los datos para agregar al nuevo beneficiario
				 | PayeeName  | PayeeAddress | Account   | Details   |
				 | AnyName    | AnyAddress   | MyAccount | AnyDetail |
   		And Confirmo agregar al beneficiario
   		Then el sistema un mensaje que se agrego correctamente

	@AddNewPayee
  	Scenario: Agregar a un beneficiario sin poner un nombre
	   	And  Me dirijo a la pagina de Pay Bills
	   	And Selecciono la opcion de  Add New Payee
	   	When Ingreso los datos para agregar al nuevo beneficiario
					 | PayeeName  | PayeeAddress | Account   | Details   |
					 | 			  | AnyAddress   | MyAccount | AnyDetail |
	   	And Confirmo agregar al beneficiario
	   	Then el sistema muestra un mensaje de error.
   
  	@PurchaseForeignCurrency
  	Scenario: Calcular costo de comprar moneda extranjera
		And Me dirijo a la pagina de Pay Bills
	   	And Selecciono la opcion de  Purchase Foreign Currency
	   	When Ingreso los datos para calcular la conversion
				| Currency  | Amount |
				| Australia | 500    |
	   	And Le doy al boton de comprar moneda extranjera
	   	Then el sistema muestra que la transaccion fue exitosa
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
	  