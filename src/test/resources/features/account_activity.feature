@AllTests
@AccountActivity
Feature: Account Activity
  Cuando quiero consultar la actividad de mi cuenta en cada uno de mis productos
  y tambien cuando quiero consultar una transacción en especifico.

	Background: Usuario se loguea en el sistema
		Given yo me autentico  en Zero Bank con el usuario 'username' y la contraseña 'password'

	@ShowTransactions
	Scenario: mostrar transacciones realizadas por tipo de producto
		And  Me dirijo a la pagina de Account Activity
	  	And Selecciono la funcionalidad de Show Transactions
	  	When Selecciono la opcion 'Loan' de la lista
	  	Then El sistema muestra las transacciones de tipo 'Loan'

	@ShowTransactions
 	Scenario: mostrar transacciones realizadas por tipo de producto, sin trasacciones
		And  Me dirijo a la pagina de Account Activity
		And Selecciono la funcionalidad de Show Transactions
		When Selecciono la opcion 'Credit Card' de la lista
		Then El sistema me muestra un mensaje de error diciendo 'No results.'

 	@FindTransactions
 	Scenario: buscar una transaccion antigua existente
		And  Me dirijo a la pagina de Account Activity
		And Selecciono la funcionalidad de Find Transactions
		When ingreso los datos de busqueda
			| Descripcion | Date       | DateTo     | Amount  | Amount2 | Type |
			| ONLINE      | 2012-09-06 | 2012-09-10 | 0       | 1000    | Any  |
		Then El sistema me muestra una transaccion con la descripcion 'ONLINE TRANSFER REF #UKKSDRQG6L'
 		
 	@FindTransactions
 	Scenario: buscar una transaccion antigua inexistente
		And Me dirijo a la pagina de Account Activity
		And Selecciono la funcionalidad de Find Transactions
		When ingreso los datos de busqueda
			| Descripcion | Date       | DateTo     | Amount  | Amount2 | Type |
			| AnyTran     | 2012-09-01 | 2012-09-01 | 0       | 0       | Any  |
		Then El sistema me muestra un mensaje de error diciendo 'No results.'
 		