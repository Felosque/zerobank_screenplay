package stepsdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import navigation.NavigateTo;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;
import questions.TransferFundQuestions;
import tasks.TransferMoney;

import java.util.List;

public class TransferFundStepsDefinitions {

    @Before
    public void setStage(){
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("Me dirijo a la pagina de Transfer Funds")
    public void me_dirijo_a_la_pagina_de_transfer_funds() {
            OnStage.theActorInTheSpotlight().attemptsTo(
                    NavigateTo.theTransferFundsPage()
            );
    }

    @When("Ingreso los datos para realizar la transaccion")
    public void ingreso_los_datos_para_realizar_la_transaccion(DataTable dataTable) {
        List<List<String>> data = dataTable.asLists();
        for (int i = 1; i < data.size(); i++){
            OnStage.theActorInTheSpotlight().attemptsTo(
                    TransferMoney.with(
                            data.get(i).get(0),
                            data.get(i).get(1),
                            data.get(i).get(2),
                            data.get(i).get(3)
                    )
            );
        }
    }
    @When("Confirmo la realizacion de la transaccion")
    public void confirmo_la_realizacion_de_la_transaccion() {
    }
    @Then("el sistema muestra el mensaje de transaccion confirmada")
    public void el_sistema_muestra_el_mensaje_de_transaccion_confirmada() {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("", TransferFundQuestions.isSuccessfullyTransaction(), Matchers.is(true))
        );
    }


    @Then("el sistema muestra un mensaje de error")
    public void el_sistema_muestra_un_mensaje_de_error() {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("", TransferFundQuestions.isSuccessfullyTransaction(), Matchers.is(false))
        );
    }

}
