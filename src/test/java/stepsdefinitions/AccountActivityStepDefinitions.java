package stepsdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import navigation.NavigateTo;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;
import questions.ActivityAccountQuestions;
import tasks.FindTransactions;
import tasks.ShowTransactions;

import java.util.List;

public class AccountActivityStepDefinitions {

    @Before
    public void setStage(){
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("Me dirijo a la pagina de Account Activity")
    public void me_dirijo_a_la_pagina_de_account_activity() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.theActivityAccountTab()
        );
    }

    @Given("Selecciono la funcionalidad de Show Transactions")
    public void selecciono_la_funcionalidad_de_show_transactions() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.theActivityAccountShowTransactions()
        );

    }

    @When("Selecciono la opcion {string} de la lista")
    public void selecciono_la_opcion_de_la_lista(String type) {
        OnStage.theActorInTheSpotlight().attemptsTo(
                ShowTransactions.with(type)
        );
    }

    @Then("El sistema muestra las transacciones de tipo {string}")
    public void el_sistema_muestra_las_transacciones_de_tipo_loan(String type) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("", ActivityAccountQuestions.isTableTransactionsVisible(), Matchers.is(true))
        );
    }

    @Then("El sistema muestra un mensaje de {string}")
    public void el_sistema_muestra_un_mensaje_de(String message) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("The displayed text", ActivityAccountQuestions.tableMessageError(), Matchers.equalTo(message))
        );
    }

    @Then("El sistema me muestra un mensaje de error diciendo {string}")
    public void el_sistema_me_muestra_un_mensaje_de_error_diciendo(String message) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("The displayed text", ActivityAccountQuestions.tableMessageError(), Matchers.equalTo(message))
        );
    }

    @Given("Selecciono la funcionalidad de Find Transactions")
    public void selecciono_la_funcionalidad_de_find_transactions() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.theActivityAccountFindTransactions()
        );
    }
    @When("ingreso los datos de busqueda")
    public void ingreso_los_datos_de_busqueda(DataTable dataTable) {
        List<List<String>> data = dataTable.asLists();
        for (int i = 1; i < data.size(); i++){
            OnStage.theActorInTheSpotlight().attemptsTo(
                    FindTransactions.with(
                            data.get(i).get(0), data.get(i).get(1),
                            data.get(i).get(2), data.get(i).get(3),
                            data.get(i).get(4), data.get(i).get(5)
                    )
            );
        }
    }
    @Then("El sistema me muestra una transaccion con la descripcion {string}")
    public void el_sistema_me_muestra_una_transaccion_con_la_descripcion(String description) {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("The displayed text", ActivityAccountQuestions.transactionDescription(), Matchers.equalTo(description))
        );
    }
}
