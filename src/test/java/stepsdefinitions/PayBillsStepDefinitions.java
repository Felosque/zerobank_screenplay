package stepsdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import navigation.NavigateTo;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;
import questions.PayBillsQuestions;
import tasks.AddNewPayee;
import tasks.MakePaymentSavedPayee;
import tasks.PurchaseForeignCurrency;

import java.util.List;

public class PayBillsStepDefinitions {

    @Before
    public void setStage(){
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("Me dirijo a la pagina de Pay Bills")
    public void me_dirijo_a_la_pagina_de_pay_bills() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.thePayBillsTab()
        );
    }

    @Given("Selecciono la opcion de  Pay Saved Payee")
    public void selecciono_la_opcion_de_pay_saved_payee() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.thePayBillsPaySaved()
        );
    }

    @When("Ingreso los datos para realizar la transaccion al beneficiario")
    public void ingreso_los_datos_para_realizar_la_transaccion_al_beneficiario(DataTable dataTable) {
        List<List<String>> data = dataTable.asLists();
        for (int i = 1; i < data.size(); i++){
            OnStage.theActorInTheSpotlight().attemptsTo(
                    MakePaymentSavedPayee.with(
                            data.get(i).get(0), data.get(i).get(1),
                            data.get(i).get(2), data.get(i).get(3),
                            data.get(i).get(4)
                    )
            );
        }
    }

    @When("Confirmo la realizacion de la transaccion al beneficiario")
    public void confirmo_la_realizacion_de_la_transaccion_al_beneficiario() {
    }

    @Then("el sistema muestra que la transaccion fue exitosa")
    public void el_sistema_muestra_que_la_transaccion_fue_exitosa() {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("", PayBillsQuestions.isSuccessfullyTransaction(), Matchers.is(true))
        );
    }

    @Then("el sistema muestra un mensaje de error.")
    public void el_sistema_muestra_un_mensaje_de_error() {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("", PayBillsQuestions.isSuccessfullyTransaction(), Matchers.is(false))
        );
    }

    @Given("Selecciono la opcion de  Add New Payee")
    public void selecciono_la_opcion_de_add_new_payee() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.thePayBillsAddNewPayee()
        );
    }
    @When("Ingreso los datos para agregar al nuevo beneficiario")
    public void ingreso_los_datos_para_agregar_al_nuevo_beneficiario(DataTable dataTable) {
        List<List<String>> data = dataTable.asLists();
        for (int i = 1; i < data.size(); i++){
            OnStage.theActorInTheSpotlight().attemptsTo(
                    AddNewPayee.with(
                            data.get(i).get(0), data.get(i).get(1),
                            data.get(i).get(2), data.get(i).get(3)
                    )
            );
        }
    }
    @When("Confirmo agregar al beneficiario")
    public void confirmo_agregar_al_beneficiario() {

    }

    @Given("Selecciono la opcion de  Purchase Foreign Currency")
    public void selecciono_la_opcion_de_purchase_foreign_currency() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                NavigateTo.thePayBillsPurchaseCurrency()
        );
    }
    @When("Ingreso los datos para calcular la conversion")
    public void ingreso_los_datos_para_calcular_la_conversion(DataTable dataTable) {
        List<List<String>> data = dataTable.asLists();
        for (int i = 1; i < data.size(); i++){
            OnStage.theActorInTheSpotlight().attemptsTo(
                    PurchaseForeignCurrency.with(
                            data.get(i).get(0), data.get(i).get(1)
                    )
            );
        }
    }
    @When("Le doy al boton de comprar moneda extranjera")
    public void le_doy_al_boton_de_comprar_moneda_extranjera() {
    }

    @Then("el sistema un mensaje que se agrego correctamente")
    public void el_sistema_un_mensaje_que_se_agrego_correctamente() {
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("", PayBillsQuestions.isSuccessfullyTransaction(), Matchers.is(true))
        );
    }

}
