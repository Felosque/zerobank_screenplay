package stepsdefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import navigation.NavigateTo;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import org.hamcrest.Matchers;
import questions.LoginQuestions;
import tasks.LoginUserTask;

public class LoginStepDefinitions {

    @Before
    public void setStage(){
        WebDriverManager.chromedriver().setup();
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("yo me autentico  en Zero Bank con el usuario {string} y la contraseña {string}")
    public void yo_me_autentico_en_zero_bank_con_el_usuario_y_la_contraseña(String username, String password) {
        OnStage.theActorCalled(username).attemptsTo(
                NavigateTo.theLoginPage(),
                LoginUserTask
                        .with()
                        .username(username)
                        .password(password)
                        .build()
        );
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat("The displayed text", LoginQuestions.titleLogIn(), Matchers.equalTo("Zero Bank"))
        );
    }
}
