
@TransferFund
Feature: Tranferir dinero a otras cuentas
  Quiero transferir dinero a otras cuentas propias

	Background: Usuario se loguea en el sistema
			Given yo me autentico  en Zero Bank con el usuario 'username' y la contraseña 'password'

  @TransferMoney
  Scenario: Tranferir un monto a otra cuenta exitosamente
   	And Me dirijo a la pagina de Transfer Funds
   	When Ingreso los datos para realizar la transaccion
					| FromAccount | ToAccount | Amount |Description |
					| Loan     | Loan      | 500    | Any Desc   |
   	And  Confirmo la realizacion de la transaccion
   	Then el sistema muestra el mensaje de transaccion confirmada
  
  @TransferMoney
  Scenario: Tranferir un monto negativo
   	And Me dirijo a la pagina de Transfer Funds
   	When Ingreso los datos para realizar la transaccion
					| FromAccount | ToAccount | Amount |Description |
					| Savings     | Loan      | -500    | Any Desc   |
   	And  Confirmo la realizacion de la transaccion
    Then el sistema muestra un mensaje de error
    
  @TransferMoney
  Scenario: Tranferir un monto superior al balance
   	And Me dirijo a la pagina de Transfer Funds
   	When Ingreso los datos para realizar la transaccion
					| FromAccount | ToAccount | Amount  | Description |
					| Savings     | Loan      | 100000  | Any Desc   |
   	And  Confirmo la realizacion de la transaccion
    Then el sistema muestra un mensaje de error
    
  @TransferMoney
  Scenario: Tranferir dejando en blanco el monto
   	And Me dirijo a la pagina de Transfer Funds
   	When Ingreso los datos para realizar la transaccion
					| FromAccount | ToAccount | Amount  | Description |
					| Savings     | Loan      | 		| Any Desc   |
   	And  Confirmo la realizacion de la transaccion
    Then el sistema muestra un mensaje de error
  
